﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{   
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }

        event ElapsedEventHandler Elapsed;
        public void TimerServiceEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Write(string logInfo)
        {
            try
            {                
                using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(logInfo);
                }
            }
            catch (Exception e)
            {
                
            }
        }
        public string Read()
        {    
            
            using (StreamReader sr = new StreamReader(LogPath, System.Text.Encoding.Default))
            {
                try
                {
                    string line;
                    string res = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        res += line + "\n";
                    }
                    return res;
                }
                catch
                {
                    throw new InvalidOperationException();
                }
            }            
        }
    }
 }