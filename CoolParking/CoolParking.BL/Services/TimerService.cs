﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;
using System;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer timer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void TimerServiceEvent()
        {
            Elapsed?.Invoke(this, null);
        }
        
        public TimerService(double interval)
        {
            Interval = interval;
        }

        public void Start()
        {
            timer = new System.Timers.Timer(Interval);           
            timer.AutoReset = true;
            timer.Enabled = true;           
        }      

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Stop();
            timer.Dispose();
        }
    }
}