﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.IO;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            
        private Parking Parking;
        private List<TransactionInfo> transactionInfoList;
        private ITimerService fakeTimerService;
        private ITimerService TimerService;
        private ILogService logService;

        public ParkingService(ITimerService timerService, ITimerService fakeTimerService, ILogService logService)
        {
            Parking =  Parking.getParking();
            //this.fakeTimerService = fakeTimerService;
            transactionInfoList = new List<TransactionInfo>();
            logService = new LogService(_logFilePath);
            this.TimerService = timerService;
            this.TimerService.Elapsed += paymantProcces;
            this.TimerService.Elapsed += FakeTimerService_Elapsed;
            this.logService = logService;

            this.fakeTimerService = fakeTimerService;
            this.fakeTimerService.Elapsed += FakeTimerService_Elapsed; //FakeTimerService_Elapsed;
        }

        private void FakeTimerService_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
        }

        public void paymantProcces(object sender, System.Timers.ElapsedEventArgs e)
        {            
            foreach (var vehicle in Parking.vehicles)
            {
                var vehicleType = vehicle.VehicleType;

                decimal incoming = Settings.rate[vehicleType];

                if ((vehicle.Balance) < 0)
                {
                    incoming = incoming * Settings.CoefficientFine;
                }

                if ((vehicle.Balance) < Settings.rate[vehicleType])
                {
                    incoming = vehicle.Balance + (Settings.rate[vehicleType] - vehicle.Balance) * Settings.CoefficientFine;
                }
                    
                Parking.Balance += incoming;
                vehicle.Balance -= incoming;
                Parking.BalancePerPeriod += incoming;

                transactionInfoList.Add(new TransactionInfo(DateTime.Now, vehicle.Id, incoming));                
            }
            
        }

        public void TransactionsWriteToLog(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var trans in transactionInfoList)
            {
                logService.Write(trans.GetStringTransactionInfo());
            }
            transactionInfoList.Clear();
        }

        public decimal GetBalance() 
        {
            //return Parking.Balance;
            return Parking.BalancePerPeriod;
        }
        public int GetCapacity()
        {
            return Parking.Capacity;
        }
        public int GetFreePlaces()
        {
            return Parking.Capacity - Parking.BusyPlaceCount;
        }

        public int GetBusyPlaceCount()
        {
            return Parking.BusyPlaceCount;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehiclesReadonly = Parking.vehicles.AsReadOnly();
            return vehiclesReadonly;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            Parking.BalancePerPeriod = 0;
            if (Parking.BusyPlaceCount == Settings.ParkingCapacity)
            {
                throw new System.InvalidOperationException();
            }

            Vehicle vehicleForAdd = Parking.vehicles.Find(i => i.Id == vehicle.Id);
            if (vehicleForAdd != null)
            {
                Console.WriteLine("Exist");
                throw new System.ArgumentException();
                
            }           
            else
            {
                Parking.vehicles.Add(vehicle);
            }
            
        }        

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicleForRemove = Parking.vehicles.Find(i => i.Id == vehicleId);
            if (vehicleForRemove != null)
            {
                Parking.vehicles.Remove(vehicleForRemove);
            }
            else
            {
                throw new System.ArgumentException();
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {                       
            Vehicle vehicleForTopUp = Parking.vehicles.Find(i => i.Id == vehicleId);

            if ((sum < 0) || (vehicleForTopUp == null))
            {
                throw new ArgumentException();
            }
            else
            {
                vehicleForTopUp.Balance += sum;
            }
            
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfoList.ToArray();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void Dispose()
        {
            Parking.vehicles.Clear();
            transactionInfoList.Clear();
            System.GC.SuppressFinalize(this);                          
        }       
    }
}