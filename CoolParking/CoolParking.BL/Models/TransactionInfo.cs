﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.Dynamic;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        DateTime dateTime;

        string id_vehicle;

        public decimal Sum { get; set; }

        public TransactionInfo(DateTime dateTime, string id_vehicle, decimal Sum)
        {
            this.dateTime = dateTime;
            this.id_vehicle = id_vehicle;
            this.Sum = Sum;
        }

        public string GetStringTransactionInfo()
        {
            return string.Format("{0} {1} {2}", dateTime, id_vehicle, Sum);
        }
    }
}