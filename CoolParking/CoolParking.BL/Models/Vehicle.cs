﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.

//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text;
using System.Linq;
using System.Diagnostics.Contracts;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get;  }
        public VehicleType VehicleType { get;  }

        public decimal Balance { get; internal set; }

        public Vehicle(string Id, VehicleType vehicleType, decimal balance)
        {
            if (isValid_id(Id) && balance > 0m)
            {
                this.Id = Id;
                this.VehicleType = vehicleType;
                this.Balance = balance;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            this.Id = GenerateRandomRegistrationPlateNumber();

            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return String.Format("{0}{1}-{2}{3}{4}{5}-{6}{7}", X(), X(), Y(), Y(), Y(), Y(), X(), X());
        }

        static string X()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random rand = new Random();
            int num = rand.Next(0, chars.Length - 1);
            return chars[num].ToString();
        }

        static string Y()
        {
            var rand = new Random();            
            int Y = rand.Next(0, 10);
            return Y.ToString();
        }

        public bool isValid_id(string id)
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string digits = "0123456789";
            char defis = '-';
            var d = (id.Length != 10) ? false : true;
            if (id.Length != 10) return false;
            if (!chars.Contains(id[0])) return false;
            if (!chars.Contains(id[1])) return false;
            if (!chars.Contains(id[8])) return false;
            if (!chars.Contains(id[9])) return false;

            if (!digits.Contains(id[3])) return false;
            if (!digits.Contains(id[4])) return false;
            if (!digits.Contains(id[5])) return false;
            if (!digits.Contains(id[6])) return false;

            if (!(id[2] == defis)) return false;
            if (!(id[7] == defis)) return false;

            return true;
        }      
    }
}
   
