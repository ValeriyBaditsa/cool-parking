﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance = 0;
        public static int ParkingCapacity = 10;
        public static int PaymentInterval = 5000;  //5
        public static double LogWriteIneraval = 60000; //60

        public static Dictionary<VehicleType, decimal> rate = new Dictionary<VehicleType, decimal> {
            {VehicleType.PassengerCar, 2m},
            {VehicleType.Truck, 5m},
            {VehicleType.Bus, 3.5m},
            {VehicleType.Motorcycle, 1m}
        };

        public static decimal CoefficientFine = 2.5m;
        
    }


}
