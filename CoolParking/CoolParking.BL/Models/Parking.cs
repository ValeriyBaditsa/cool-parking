﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public List<Vehicle> vehicles;
        public void Dispose()
        {           
            GC.SuppressFinalize(this);
            vehicles.Clear();
        }        
        
        public decimal BalancePerPeriod { get; set; }
        

        public decimal Balance { get; set; }
        private Parking() 
        {
            vehicles = new List<Vehicle>();
            capacity = Settings.ParkingCapacity;
        }

        private int busyPlacesCount;
        public int BusyPlaceCount 
        {
            get { return busyPlacesCount; }
            set { busyPlacesCount = value; }
        }

        private int capacity;
        public int Capacity
        {
            get { return capacity; }
            set { if (capacity >= 0) capacity = value; }
        }

        private static Parking instance;
        public static Parking getParking()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }
    }
}
