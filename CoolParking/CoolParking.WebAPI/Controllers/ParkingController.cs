﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Services;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private Parking_Services parkingService;

        public ParkingController(Parking_Services service)
        {
            //parkingService = new Parking_Services();
            parkingService = service;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<Parking>>> Get()
        {
            return Ok(await parkingService.GetParking());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Parking>> Get(int id)
        {
            return Ok(await parkingService.GetParking(id));
        }
    }

}