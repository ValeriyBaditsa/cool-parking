﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Services
{
    public class Parking_Services
    {
        private HttpClient _client;

        public Parking_Services()
        {
            _client = new HttpClient();
        }

        public async Task<List<Parking>> GetParking()
        {
            var users = await _client. GetStringAsync("https://jsonplaceholder.typicode.com/users");
            return JsonConvert.DeserializeObject<List<Parking>>(users);
        }

        public async Task<Parking> GetParking(int id)
        {
            var user = await _client.GetStringAsync($"https://jsonplaceholder.typicode.com/users/{id}");
            return JsonConvert.DeserializeObject<Parking>(user);
        }
    }
}
