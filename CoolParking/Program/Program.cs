﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Tests;
using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;
using System.Timers;
using System.Linq;

namespace Program
{
    class Program
    {        
        public static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("test t");
        }

        static void Main(string[] args)
        {
            string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

            TimerService _withdrawTimer = new TimerService(Settings.PaymentInterval);
            TimerService _logTimer = new TimerService(Settings.LogWriteIneraval);
            ILogService _logService = new LogService(_logFilePath);                              
           
            ParkingService parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            parkingService.AddVehicle(new Vehicle(VehicleType.Truck, 100));
            parkingService.AddVehicle(new Vehicle(VehicleType.Bus, 100));
            parkingService.AddVehicle(new Vehicle(VehicleType.Motorcycle, 100));
            parkingService.AddVehicle(new Vehicle(VehicleType.PassengerCar, 100));

            _withdrawTimer.Start();
            _withdrawTimer.timer.Elapsed += parkingService.paymantProcces;

            _logTimer.Start();
            _logTimer.timer.Elapsed += parkingService.TransactionsWriteToLog;

            Console.WriteLine("Wtlcome to Cool parking! Please enter");
            Console.WriteLine("1 - for get current parking balance");
            Console.WriteLine("2 - for current earned money");
            Console.WriteLine("3 - for free/busy places");
            Console.WriteLine("4 - for get last parking Transactions");
            Console.WriteLine("5 - for get last parking Transactions");
            Console.WriteLine("6 - for add Bus Transactions");
            Console.WriteLine("0 - Exit");

            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Current balance: {0}", parkingService.GetBalance());
                        break;
                    case "2":
                        {
                            var lastParkingTransactions = parkingService.GetLastParkingTransactions();
                            Console.WriteLine("Current earned money: {0}", lastParkingTransactions.Sum(tr => tr.Sum));
                        }
                        break;
                    case "3":
                        Console.WriteLine("Free/busy places: {0}/{1}", parkingService.GetFreePlaces(),
                            parkingService.GetBusyPlaceCount());
                        break;
                    case "4":
                        {
                            Console.WriteLine("Get last parking Transactions: {0}", 
                                parkingService.GetLastParkingTransactions());
                        }
                        break;
                    case "5":
                        {
                            foreach (var v in parkingService.GetVehicles())
                            {
                                Console.WriteLine("{0}", v.VehicleType);
                            }                           
                        }
                        break;

                    case "6":
                        {
                            parkingService.AddVehicle(new Vehicle(VehicleType.Bus, 60)) ;
                            Console.WriteLine("Vahicle has been added");
                            break;
                        }
                    case "0":
                        Console.Write("Press any key to close app...");                        
                        return;                       
                }
            }            
        }
    }   
}
    